
#include "color/Colors.h"
#include "gtest/gtest.h"

#include <set>
#include <stdexcept>

namespace color {

TEST(ColorsTest, Off) {
    EXPECT_EQ(Colors::Off, std::string("\x1b[0m"));
}

TEST(ColorsTest, Fg) {
    EXPECT_EQ(Colors::fg(Palette::Red), "\x1b[38;5;1m");
    EXPECT_EQ(Colors::fg(Palette::Blue, Attribute::Normal), "\x1b[38;5;4m");
    EXPECT_EQ(Colors::fg(Palette::Cyan, Attribute::Bold), "\x1b[1;38;5;6m");
    EXPECT_EQ(Colors::fg(Palette::Green, Attribute::Underline), "\x1b[4;38;5;2m");
    EXPECT_EQ(Colors::fg(Palette::Magenta, Attribute::BoldUnderline), "\x1b[1;4;38;5;5m");
}

TEST(ColorsTest, Bg) {
    EXPECT_EQ(Colors::bg(Palette::White), "\x1b[48;5;7m");
}
}
