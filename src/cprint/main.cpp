#include "parser/Builder.h"

#include <cstring>
#include <getopt.h>
#include <iostream>
#include <string>

namespace {
void usage(const char* app) {
    std::cerr << "Usage: " << ::basename(app)
              << " [-p|-h|-c <cofig file>] \n";
    std::cerr << "Options:\n"
                 "  -c, --config=config-file   specify configuration file to use\n"
                 "  -p, --print-colors         print available colors\n"
                 "  -h, --help                 display this message\n";
}

void process_parameters(int argc, char* argv[], bool& print_colors, bool& help, std::string& conf_file) {
    print_colors = help = false;
    struct option long_options[] = {
        { "print-colors", no_argument, nullptr, 'p' },
        { "help", no_argument, nullptr, 'h' },
        { "config", required_argument, nullptr, 'c' },
        { nullptr, 0, nullptr, 0 },
    };
    int c;
    while ((c = getopt_long(argc, argv, "phc:", long_options, nullptr)) != -1) {
        switch (c) {
            case 'p':
                print_colors = true;
                break;
            case 'h':
                help = true;
                break;
            case 'c':
                conf_file = optarg;
                break;
            case '?':
                usage(argv[0]);
                exit(1);
            default:
                break;
        }
    }
}
}

int main(int argc, char* argv[]) {
    bool print_colors, help;
    std::string conf_file;
    process_parameters(argc, argv, print_colors, help, conf_file);

    if (print_colors) {
        parser::Builder::print_colors(std::cout);
    } else if (help) {
        usage(argv[0]);
    } else if (not conf_file.empty()) {
        const auto parser = parser::Builder::build(conf_file);
        std::string line;
        while (std::getline(std::cin, line)) {
            parser.apply(line);
            std::cout << line << std::endl;
        }
    } else {
        std::cerr << "Missing configuration file\n";
        usage(argv[0]);
        return 1;
    }
    return 0;
}
