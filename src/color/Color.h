#pragma once

#include "color/Colors.h"

#include <string>

namespace color {
class Color {
public:
    Color(Palette fgcolor, color::Attribute attr);
    Color(Palette fgcolor, color::Attribute attr, Palette bgcolor);
    explicit Color(Palette bgcolor);
    const std::string& operator()() const { return format; }

private:
    std::string format;
};
}
