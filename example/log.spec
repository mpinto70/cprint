# you must specify 'regex=' and 'format='
# format must be specified as:
#    <color name>,<style>                       --> for foreground only
#    <color name>                               --> for background only
#    <fg color name>,<style>,<bg color name>    --> for foreground and background
# style must be specified as (only for foreground):
#    Normal
#    Bold
#    Underline
#    Bold+Underline
# blanck lines and lines starting with # are ignored

# the order is from the most critical to the less critical
# currently the formatter will stop at the first match

# this specifies a line that contains ERROR in the middle and will print it in
# red on magenta
regex=^.*(MULTIPLE).*(middle).*$
format=White,Bold,Blue_4
format=Yellow_4,Bold

# this specifies a line that contains ERROR in the middle and will print it in
# red on magenta
regex=^.*ERROR.*
format=Red_4,Normal,Magenta_9

# this specifies a line that contains WARN in the middle and will print it in
# yellow bold
regex=^.*WARN.*$
format=Yellow_2,Bold

# this specifies a line that contains INFO in the middle and will print it in
# blue
regex=^.*INFO.*$
format=Blue_9,Normal

# this specifies a line that contains DEBUG in the middle and will print it in
# green
regex=^.*DEBUG.*$
format=Green_1,Normal
