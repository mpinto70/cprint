#include "Colors.h"

#include <array>
#include <vector>

namespace color {
namespace {

class ColorFormat {
public:
    ColorFormat() {
        const std::string ACTIVATION = "\x1b[";
        const std::string FOREGROUND = "38;5;";
        const std::string BACKGROUND = "48;5;";
        const std::string ATTRIBUTES[] = { "", "1;", "4;", "1;4;" };
        for (size_t c = 0; c < 256; ++c) {
            const std::string colour = std::to_string(c) + "m";
            for (size_t a = 0; a < 4; ++a) {
                foregrounds[a][c] = ACTIVATION + ATTRIBUTES[a] + FOREGROUND + colour;
            }
            backgrounds[c] = ACTIVATION + BACKGROUND + colour;
        }
    }

    const std::string& fg(Palette colour, Attribute attribute) const {
        const auto c = static_cast<size_t>(colour);
        const auto a = static_cast<size_t>(attribute);
        return foregrounds[a][c];
    }

    const std::string& bg(Palette colour) const {
        const auto c = static_cast<size_t>(colour);
        return backgrounds[c];
    }

private:
    std::array<std::array<std::string, 256>, 4> foregrounds;
    std::array<std::string, 256> backgrounds;
};

const ColorFormat color_format;
}

const std::string Colors::Off = "\x1b[0m";

const std::string& Colors::fg(Palette colour, Attribute attr) {
    return color_format.fg(colour, attr);
}

const std::string& Colors::bg(Palette colour) {
    return color_format.bg(colour);
}
}
