
#include "color/Color.h"

#include "gtest/gtest.h"

namespace color {

TEST(ColorTest, Fg) {
    Color color(Palette::Yellow_4, color::Attribute::Bold);
    const std::string cl = color::Colors::fg(Palette::Yellow_4, color::Attribute::Bold);
    EXPECT_EQ(cl, color());
}

TEST(ColorTest, FgBg) {
    Color color(Palette::Red_2, color::Attribute::Underline, Palette::Red_1);
    const std::string cl = color::Colors::fg(Palette::Red_2, color::Attribute::Underline) + color::Colors::bg(Palette::Red_1);
    EXPECT_EQ(cl, color());
}

TEST(ColorTest, Bg) {
    Color color(Palette::Blue_5);
    const std::string cl = color::Colors::bg(Palette::Blue_5);
    EXPECT_EQ(cl, color());
}
}
