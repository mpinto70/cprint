#include "Highlighter.h"

#include <stdexcept>

namespace parser {
Highlighter::Highlighter(color::Palette fgcolor, color::Attribute attr)
      : formatter(color::Colors::fg(fgcolor, attr)) {
}

Highlighter::Highlighter(color::Palette fgcolor, color::Attribute attr, color::Palette bgcolor)
      : formatter(color::Colors::fg(fgcolor, attr) + color::Colors::bg(bgcolor)) {
}

Highlighter::Highlighter(color::Palette bgcolor)
      : formatter(color::Colors::bg(bgcolor)) {
}

bool Highlighter::apply(std::string& line, const std::pair<size_t, size_t>& match) const {
    if (match.first > match.second) {
        throw std::invalid_argument("{Highlighter} - invalid match - inverted");
    }
    if (match.first == std::string::npos && match.second != std::string::npos) {
        throw std::invalid_argument("{Highlighter} - invalid match - infinity at begin");
    }
    if (match.second == std::string::npos && match.first != std::string::npos) {
        throw std::invalid_argument("{Highlighter} - invalid match - infinity at end");
    }

    // either both are std::string::npos or none are
    if (match.first == std::string::npos) {
        return false;
    }
    if (match.second > line.size()) {
        throw std::invalid_argument("{Highlighter} - invalid match - buffer overflow");
    }

    if (match.first != match.second) {
        line.insert(match.first, formatter);
        line.insert(match.second + formatter.size(), color::Colors::Off);
    }
    return true;
}
}
