#pragma once

#include <regex>
#include <string>

namespace parser {
class Matcher {
public:
    explicit Matcher(const std::string& regex);
    /** search for the regex specified in construction
     * @param line the line being searched
     * @return a vector with <begin, end> of matching regex
     */
    std::vector<std::pair<size_t, size_t>> multi_check(const std::string& line) const;

    size_t number_of_expressions() const {
        return regex.mark_count() == 0 ? 1 : regex.mark_count();
    }

private:
    std::regex regex;
};
}
