#include "Color.h"

namespace color {
Color::Color(Palette fgcolor, color::Attribute attr)
      : format(color::Colors::fg(fgcolor, attr)) {
}

Color::Color(Palette fgcolor, color::Attribute attr, Palette bgcolor)
      : format(color::Colors::fg(fgcolor, attr) + color::Colors::bg(bgcolor)) {
}

Color::Color(Palette bgcolor)
      : format(color::Colors::bg(bgcolor)) {
}
}
