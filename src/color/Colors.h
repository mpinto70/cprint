#pragma once

#include <string>

namespace color {
enum class Attribute {
    Normal,
    Bold,
    Underline,
    BoldUnderline,
};

/// color map extracted from 256 color section of http://misc.flogisoft.com/bash/tip_colors_and_formatting
enum class Palette {
    Gray = 0,
    Red = 1,
    Green = 2,
    Yellow = 3,
    Blue = 4,
    Magenta = 5,
    Cyan = 6,
    White = 7,
    GrayLight = 8,
    RedLight = 9,
    GreenLight = 10,
    YellowLight = 11,
    BlueLight = 12,
    MagentaLight = 13,
    CyanLight = 14,
    WhiteLight = 15,
    // special colors
    Black = 16,
    Blue_1 = 17,
    Blue_2 = 18,
    Blue_3 = 19,
    Blue_4 = 20,
    Blue_5 = 21,
    Blue_6 = 27,
    Blue_7 = 33,
    Blue_8 = 39,
    Blue_9 = 75,
    Cyan_1 = 45,
    Cyan_2 = 51,
    Cyan_3 = 87,
    Cyan_4 = 123,
    Gray_1 = 233,
    Gray_2 = 235,
    Gray_3 = 237,
    Gray_4 = 239,
    Gray_5 = 241,
    Gray_6 = 243,
    Gray_7 = 245,
    Gray_8 = 247,
    Green_1 = 22,
    Green_2 = 28,
    Green_3 = 34,
    Green_4 = 40,
    Green_5 = 70,
    Green_6 = 76,
    Green_7 = 82,
    Green_8 = 118,
    Magenta_1 = 53,
    Magenta_2 = 57,
    Magenta_3 = 93,
    Magenta_4 = 129,
    Magenta_5 = 165,
    Magenta_6 = 201,
    Magenta_7 = 207,
    Magenta_8 = 213,
    Magenta_9 = 219,
    Red_1 = 52,
    Red_2 = 88,
    Red_3 = 124,
    Red_4 = 160,
    Red_5 = 196,
    Yellow_1 = 202,
    Yellow_2 = 208,
    Yellow_3 = 214,
    Yellow_4 = 220,
    Yellow_5 = 226,
};

class Colors {
public:
    static const std::string Off;
    static const std::string& fg(Palette colour, Attribute attr = Attribute::Normal);
    static const std::string& bg(Palette colour);
};
}
