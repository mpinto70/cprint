
#include "parser/Parser.h"

#include "gtest/gtest.h"

namespace parser {

TEST(ParserTest, apply) {
    std::vector<Formatter> formatters = {
        { Matcher{ "D_123" }, { { color::Palette::Yellow_4, color::Attribute::Bold } } },
        { Matcher{ "C_12" }, { { color::Palette::Yellow_3, color::Attribute::Underline } } },
        { Matcher{ "B_1" }, { { color::Palette::Yellow_2, color::Attribute::BoldUnderline } } },
        { Matcher{ "A" }, { { color::Palette::Yellow_1, color::Attribute::Normal } } },
    };

    Parser parser(std::move(formatters));

    const std::string yellow_4 = color::Colors::fg(color::Palette::Yellow_4, color::Attribute::Bold);
    const std::string yellow_3 = color::Colors::fg(color::Palette::Yellow_3, color::Attribute::Underline);
    const std::string yellow_2 = color::Colors::fg(color::Palette::Yellow_2, color::Attribute::BoldUnderline);
    const std::string yellow_1 = color::Colors::fg(color::Palette::Yellow_1, color::Attribute::Normal);
    const std::string formatted_4 = "something " + yellow_4 + "D_123" + color::Colors::Off + " something";
    const std::string formatted_3 = "something " + yellow_3 + "C_12" + color::Colors::Off + " something";
    const std::string formatted_2 = "something " + yellow_2 + "B_1" + color::Colors::Off + " something";
    const std::string formatted_1 = "something " + yellow_1 + "A" + color::Colors::Off + " something";

    std::string line = "something D_123 something";
    parser.apply(line);
    EXPECT_EQ(line, formatted_4);

    line = "something C_12 something";
    parser.apply(line);
    EXPECT_EQ(line, formatted_3);

    line = "something B_1 something";
    parser.apply(line);
    EXPECT_EQ(line, formatted_2);

    line = "something A something";
    parser.apply(line);
    EXPECT_EQ(line, formatted_1);
}

TEST(ParserTest, apply_multiple) {
    std::vector<Formatter> formatters = {
        { Matcher{ "D_123" }, { { color::Palette::Yellow_4, color::Attribute::Bold } } },
        { Matcher{ "C_12" }, { { color::Palette::Yellow_3, color::Attribute::Underline } } },
        { Matcher{ "B_1" }, { { color::Palette::Yellow_2, color::Attribute::BoldUnderline } } },
        { Matcher{ "A" }, { { color::Palette::Yellow_1, color::Attribute::Normal } } },
    };

    Parser parser(std::move(formatters));

    const std::string yellow_4 = color::Colors::fg(color::Palette::Yellow_4, color::Attribute::Bold);
    const std::string yellow_3 = color::Colors::fg(color::Palette::Yellow_3, color::Attribute::Underline);
    const std::string yellow_2 = color::Colors::fg(color::Palette::Yellow_2, color::Attribute::BoldUnderline);
    const std::string yellow_1 = color::Colors::fg(color::Palette::Yellow_1, color::Attribute::Normal);
    const std::string formatted = "something "
                                  + yellow_4 + "D_123" + color::Colors::Off + " "
                                  + yellow_3 + "C_12" + color::Colors::Off + " "
                                  + yellow_2 + "B_1" + color::Colors::Off + " "
                                  + yellow_1 + "A" + color::Colors::Off + " "
                                  + "something";

    std::string line = "something D_123 C_12 B_1 A something";
    parser.apply(line);
    EXPECT_EQ(line, formatted);
}

TEST(ParserTest, multiple_matches) {
    std::vector<Formatter> formatters = {
        { Matcher{ "(A_1).*(B_2)" }, { { color::Palette::Yellow_4, color::Attribute::Bold }, { color::Palette::Blue_5, color::Attribute::Normal } } },
    };

    Parser parser(std::move(formatters));

    const std::string yellow_4 = color::Colors::fg(color::Palette::Yellow_4, color::Attribute::Bold);
    const std::string blue_5 = color::Colors::fg(color::Palette::Blue_5, color::Attribute::Normal);
    const std::string formatted = "something "
                                  + yellow_4 + "A_1" + color::Colors::Off + " something else "
                                  + blue_5 + "B_2" + color::Colors::Off + " "
                                  + "something end";

    std::string line = "something A_1 something else B_2 something end";
    parser.apply(line);
    EXPECT_EQ(line, formatted);
}
}
