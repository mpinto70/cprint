#include "parser/Highlighter.h"

#include "gtest/gtest.h"

namespace parser {

TEST(HighlighterTest, ApplyNone) {
    Highlighter formatter(color::Palette::Yellow_4, color::Attribute::Bold);
    const std::string yellow_4 = color::Colors::fg(color::Palette::Yellow_4, color::Attribute::Bold);
    std::string line = "0123456789";
    const std::string formatted = line;
    EXPECT_FALSE(formatter.apply(line, std::make_pair(std::string::npos, std::string::npos)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(0, 0)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(1, 1)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(2, 2)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(3, 3)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(4, 4)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(5, 5)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(6, 6)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(7, 7)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(8, 8)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(9, 9)));
    EXPECT_TRUE(formatter.apply(line, std::make_pair(10, 10)));
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyError) {
    Highlighter formatter(color::Palette::Yellow_4, color::Attribute::Bold);
    const std::string color = color::Colors::fg(color::Palette::Yellow_4, color::Attribute::Bold);
    std::string line = "0123456789";
    const std::string formatted = line;
    EXPECT_THROW(formatter.apply(line, std::make_pair(std::string::npos, 10)), std::invalid_argument);
    EXPECT_THROW(formatter.apply(line, std::make_pair(0, std::string::npos)), std::invalid_argument);
    EXPECT_THROW(formatter.apply(line, std::make_pair(0, 11)), std::invalid_argument);
    EXPECT_THROW(formatter.apply(line, std::make_pair(11, 11)), std::invalid_argument);
    EXPECT_THROW(formatter.apply(line, std::make_pair(4, 3)), std::invalid_argument);
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyFullFg) {
    Highlighter formatter(color::Palette::Yellow_4, color::Attribute::Bold);
    const std::string color = color::Colors::fg(color::Palette::Yellow_4, color::Attribute::Bold);
    std::string line = "0123456789";
    const std::string formatted = color + "0123456789" + color::Colors::Off;
    EXPECT_TRUE(formatter.apply(line, std::make_pair(0, 10)));
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyMiddleFg) {
    Highlighter formatter(color::Palette::Red_3, color::Attribute::Underline);
    const std::string color = color::Colors::fg(color::Palette::Red_3, color::Attribute::Underline);
    std::string line = "0123456789";
    const std::string formatted = "012" + color + "345" + color::Colors::Off + "6789";
    EXPECT_TRUE(formatter.apply(line, std::make_pair(3, 6)));
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyFullBg) {
    Highlighter formatter(color::Palette::Black);
    const std::string color = color::Colors::bg(color::Palette::Black);
    std::string line = "0123456789";
    const std::string formatted = color + "0123456789" + color::Colors::Off;
    EXPECT_TRUE(formatter.apply(line, std::make_pair(0, 10)));
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyMiddleBg) {
    Highlighter formatter(color::Palette::White);
    const std::string color = color::Colors::bg(color::Palette::White);
    std::string line = "0123456789";
    const std::string formatted = "012" + color + "345" + color::Colors::Off + "6789";
    EXPECT_TRUE(formatter.apply(line, std::make_pair(3, 6)));
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyFullFgBg) {
    Highlighter formatter(color::Palette::Red_4, color::Attribute::BoldUnderline, color::Palette::Blue_1);
    const std::string color = color::Colors::fg(color::Palette::Red_4, color::Attribute::BoldUnderline) + color::Colors::bg(color::Palette::Blue_1);
    std::string line = "0123456789";
    const std::string formatted = color + "0123456789" + color::Colors::Off;
    EXPECT_TRUE(formatter.apply(line, std::make_pair(0, 10)));
    EXPECT_EQ(line, formatted);
}

TEST(HighlighterTest, ApplyMiddleFgBg) {
    Highlighter formatter(color::Palette::Red_1, color::Attribute::BoldUnderline, color::Palette::Blue_2);
    const std::string color = color::Colors::fg(color::Palette::Red_1, color::Attribute::BoldUnderline) + color::Colors::bg(color::Palette::Blue_2);
    std::string line = "0123456789";
    const std::string formatted = "012" + color + "345" + color::Colors::Off + "6789";
    EXPECT_TRUE(formatter.apply(line, std::make_pair(3, 6)));
    EXPECT_EQ(line, formatted);
}
}
