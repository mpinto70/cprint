#pragma once

#include "parser/Highlighter.h"
#include "parser/Matcher.h"

#include <vector>

namespace parser {
struct Formatter {
    Formatter(Matcher matcher_, std::vector<Highlighter> highlighters_)
          : matcher(std::move(matcher_)),
            highlighters(std::move(highlighters_)) {
        if (matcher.number_of_expressions() != highlighters.size())
            throw std::invalid_argument("Number of expressions ("
                                        + std::to_string(matcher.number_of_expressions())
                                        + ") is different from the number of highlighters ("
                                        + std::to_string(highlighters.size())
                                        + ")");
    }

    Matcher matcher;
    std::vector<Highlighter> highlighters;
};

class Parser {
public:
    explicit Parser(std::vector<Formatter>&& formatters);

    void apply(std::string& line) const;

private:
    std::vector<Formatter> formatters;
};
}
