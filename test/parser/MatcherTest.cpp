
#include "parser/Matcher.h"

#include "gtest/gtest.h"

namespace parser {

TEST(MathcerTest, multi_check_not_found) {
    Matcher rule("^.*MHPA.*$");
    EXPECT_EQ(rule.number_of_expressions(), 1u);
    const std::string line = "no substring here";
    EXPECT_EQ(rule.multi_check(line).size(), 0u);
}

TEST(MathcerTest, multi_check_found_1) {
    Matcher rule("^.*MHPA.*$");
    EXPECT_EQ(rule.number_of_expressions(), 1u);
    const std::string line = "something MHPA something";
    const std::vector<std::pair<size_t, size_t>> expected{ { 0, line.size() } };
    EXPECT_EQ(rule.multi_check(line), expected);
}

TEST(MathcerTest, multi_check_found_with_subexpression) {
    Matcher rule("^.*MHPA(.*)$");
    EXPECT_EQ(rule.number_of_expressions(), 1u);
    const std::string line = "something MHPA something";
    const std::vector<std::pair<size_t, size_t>> expected{
        { 14, line.size() },
    };
    EXPECT_EQ(rule.multi_check(line), expected);
}

TEST(MathcerTest, multi_check_found_with_3_subexpression) {
    Matcher rule("^(.*)(MHPA)(.*)$");
    EXPECT_EQ(rule.number_of_expressions(), 3u);
    const std::string line = "something MHPA something";
    const std::vector<std::pair<size_t, size_t>> expected{
        { 0, 10 },
        { 10, 14 },
        { 14, line.size() },
    };
    EXPECT_EQ(rule.multi_check(line), expected);
}
}
