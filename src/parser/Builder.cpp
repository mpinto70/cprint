#include "Builder.h"

#include <fstream>
#include <string>

namespace parser {
namespace {
constexpr const char REGEX_MARK[] = "regex=";
constexpr size_t REGEX_MARK_SIZE = sizeof(REGEX_MARK) - 1;
constexpr const char FORMAT_MARK[] = "format=";
constexpr size_t FORMAT_MARK_SIZE = sizeof(FORMAT_MARK) - 1;

bool read_next_line(std::ifstream& in, std::string& line) {
    while (std::getline(in, line)) {
        if (not line.empty() && line[0] != '#') {
            return true;
        }
    }
    return false;
}

bool read_next_regex(std::ifstream& in, std::string& line) {
    if (read_next_line(in, line)) {
        return line.size() >= REGEX_MARK_SIZE && line.compare(0, REGEX_MARK_SIZE, REGEX_MARK) == 0;
    }
    return false;
}

bool read_next_format(std::ifstream& in, std::string& line) {
    if (read_next_line(in, line)) {
        return line.size() >= FORMAT_MARK_SIZE && line.compare(0, FORMAT_MARK_SIZE, FORMAT_MARK) == 0;
    }
    return false;
}

std::vector<std::string> split(const std::string& line) {
    std::vector<std::string> res;
    auto pos = size_t(-1);
    do {
        size_t start = pos + 1;
        pos = line.find(',', start);
        res.push_back(line.substr(start, pos - start));
    } while (pos != std::string::npos);
    return res;
}

color::Attribute create_attribute(const std::string& line) {
    if (line == "Normal") {
        return color::Attribute::Normal;
    } else if (line == "Bold") {
        return color::Attribute::Bold;
    } else if (line == "Underline") {
        return color::Attribute::Underline;
    } else if (line == "Bold+Underline") {
        return color::Attribute::BoldUnderline;
    } else {
        throw std::runtime_error("{Builder} invalid attribute [" + line + "]");
    }
}

std::map<std::string, color::Palette> MAPPING = {
    // basic colors
    { "Gray", color::Palette::Gray },
    { "Red", color::Palette::Red },
    { "Green", color::Palette::Green },
    { "Yellow", color::Palette::Yellow },
    { "Blue", color::Palette::Blue },
    { "Magenta", color::Palette::Magenta },
    { "Cyan", color::Palette::Cyan },
    { "White", color::Palette::White },
    { "GrayLight", color::Palette::GrayLight },
    { "RedLight", color::Palette::RedLight },
    { "GreenLight", color::Palette::GreenLight },
    { "YellowLight", color::Palette::YellowLight },
    { "BlueLight", color::Palette::BlueLight },
    { "MagentaLight", color::Palette::MagentaLight },
    { "CyanLight", color::Palette::CyanLight },
    { "WhiteLight", color::Palette::WhiteLight },
    // special colors
    { "Black", color::Palette::Black },
    { "Blue_1", color::Palette::Blue_1 },
    { "Blue_2", color::Palette::Blue_2 },
    { "Blue_3", color::Palette::Blue_3 },
    { "Blue_4", color::Palette::Blue_4 },
    { "Blue_5", color::Palette::Blue_5 },
    { "Blue_6", color::Palette::Blue_6 },
    { "Blue_7", color::Palette::Blue_7 },
    { "Blue_8", color::Palette::Blue_8 },
    { "Blue_9", color::Palette::Blue_9 },
    { "Cyan_1", color::Palette::Cyan_1 },
    { "Cyan_2", color::Palette::Cyan_2 },
    { "Cyan_3", color::Palette::Cyan_3 },
    { "Cyan_4", color::Palette::Cyan_4 },
    { "Gray_1", color::Palette::Gray_1 },
    { "Gray_2", color::Palette::Gray_2 },
    { "Gray_3", color::Palette::Gray_3 },
    { "Gray_4", color::Palette::Gray_4 },
    { "Gray_5", color::Palette::Gray_5 },
    { "Gray_6", color::Palette::Gray_6 },
    { "Gray_7", color::Palette::Gray_7 },
    { "Gray_8", color::Palette::Gray_8 },
    { "Green_1", color::Palette::Green_1 },
    { "Green_2", color::Palette::Green_2 },
    { "Green_3", color::Palette::Green_3 },
    { "Green_4", color::Palette::Green_4 },
    { "Green_5", color::Palette::Green_5 },
    { "Green_6", color::Palette::Green_6 },
    { "Green_7", color::Palette::Green_7 },
    { "Green_8", color::Palette::Green_8 },
    { "Magenta_1", color::Palette::Magenta_1 },
    { "Magenta_2", color::Palette::Magenta_2 },
    { "Magenta_3", color::Palette::Magenta_3 },
    { "Magenta_4", color::Palette::Magenta_4 },
    { "Magenta_5", color::Palette::Magenta_5 },
    { "Magenta_6", color::Palette::Magenta_6 },
    { "Magenta_7", color::Palette::Magenta_7 },
    { "Magenta_8", color::Palette::Magenta_8 },
    { "Magenta_9", color::Palette::Magenta_9 },
    { "Red_1", color::Palette::Red_1 },
    { "Red_2", color::Palette::Red_2 },
    { "Red_3", color::Palette::Red_3 },
    { "Red_4", color::Palette::Red_4 },
    { "Red_5", color::Palette::Red_5 },
    { "Yellow_1", color::Palette::Yellow_1 },
    { "Yellow_2", color::Palette::Yellow_2 },
    { "Yellow_3", color::Palette::Yellow_3 },
    { "Yellow_4", color::Palette::Yellow_4 },
    { "Yellow_5", color::Palette::Yellow_5 },
};

color::Palette map_color(const std::string& name) {
    const auto it = MAPPING.find(name);
    if (it == MAPPING.end()) {
        throw std::invalid_argument("color not found [" + name + "]");
    }
    return it->second;
}

Highlighter create_highlighter(const std::string& line) {
    const auto tokens = split(line);
    switch (tokens.size()) {
        case 1:
            return Highlighter(map_color(tokens[0]));
        case 2:
            return Highlighter(map_color(tokens[0]), create_attribute(tokens[1]));
        case 3:
            return Highlighter(map_color(tokens[0]), create_attribute(tokens[1]), map_color(tokens[2]));
        default:
            throw std::runtime_error("{Builder} invalid format specification [" + line + "]");
    }
}
}

Parser Builder::build(const std::string& file) {
    std::ifstream in(file);
    if (!in) {
        throw std::runtime_error("{Builder} it was not possible to open [" + file + "]");
    }

    std::vector<Formatter> formatters;
    std::string line;
    while (in) {
        std::string last_good_line = line;
        if (!read_next_regex(in, line)) {
            if (!in) { // eof
                break;
            } else {
                throw std::runtime_error("{Builder} invalid configuration file [regex] near [" + last_good_line + "]");
            }
        }
        last_good_line = line;
        const std::string regex = line.substr(REGEX_MARK_SIZE);
        const Matcher matcher(regex);
        std::vector<Highlighter> highlighters;
        for (size_t i = 0; i < matcher.number_of_expressions(); ++i) {
            if (!read_next_format(in, line)) {
                if (!in) { // eof
                    throw std::runtime_error(
                          "{Builder} invalid configuration file [format] near [" + last_good_line + "]");
                }
            }
            highlighters.push_back(create_highlighter(line.substr(FORMAT_MARK_SIZE)));
        }
        formatters.emplace_back(matcher, std::move(highlighters));
    }
    return Parser(std::move(formatters));
}

std::ostream& Builder::print_colors(std::ostream& out) {
    for (const auto& it : MAPPING) {
        out << " " << color::Colors::bg(it.second) << " background " << color::Colors::Off;
        out << " " << color::Colors::fg(it.second, color::Attribute::Normal) << " Normal " << color::Colors::Off;
        out << " " << color::Colors::fg(it.second, color::Attribute::Bold) << " Bold " << color::Colors::Off;
        out << " " << color::Colors::fg(it.second, color::Attribute::Underline) << " Underline " << color::Colors::Off;
        out << " " << color::Colors::fg(it.second, color::Attribute::BoldUnderline) << " Bold+Underline " << color::Colors::Off;
        out << " = " << it.first << "\n";
    }
    return out;
}
}
