#pragma once

#include "color/Colors.h"

#include <string>

namespace parser {
class Highlighter {
public:
    Highlighter(color::Palette fgcolor, color::Attribute attr);
    Highlighter(color::Palette fgcolor, color::Attribute attr, color::Palette bgcolor);
    explicit Highlighter(color::Palette bgcolor);
    bool apply(std::string& line, const std::pair<size_t, size_t>& match) const;

private:
    std::string formatter;
};
}
