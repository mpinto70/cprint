#include "Parser.h"

namespace parser {
Parser::Parser(std::vector<Formatter>&& formatters)
      : formatters(std::move(formatters)) {
}

void Parser::apply(std::string& line) const {
    for (const auto& formatter : formatters) {
        const auto match = formatter.matcher.multi_check(line);
        // apply the formatting backwards, because the line is changed inline
        for (int i = int(match.size()) - 1; i >= 0; --i) {
            formatter.highlighters[i].apply(line, match[i]);
        }
    }
}
}
