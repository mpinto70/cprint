#pragma once

#include "parser/Parser.h"

#include <ostream>

namespace parser {
class Builder {
public:
    static Parser build(const std::string& file);
    static std::ostream& print_colors(std::ostream& out);
};
}
