#include "parser/Matcher.h"

namespace parser {
Matcher::Matcher(const std::string& regex)
      : regex(regex) {
}

std::vector<std::pair<size_t, size_t>> Matcher::multi_check(const std::string& line) const {
    std::smatch match;

    if (std::regex_search(line, match, regex)) {
        std::vector<std::pair<size_t, size_t>> res;
        for (size_t m = 0; m < match.size(); ++m) {
            res.emplace_back(match.position(m), match.position(m) + match.length(m));
        }
        if (res.size() > 1) {
            // if there is more than one, the first one is the whole match
            res.erase(res.begin());
        }
        return res;
    } else {
        return {};
    }
}
}
